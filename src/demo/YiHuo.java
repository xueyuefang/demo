package demo;



public class YiHuo {
	
	public static void main(String[] args) {
		String ss="55AA20000201049A2D15C6";
		String yiHuo = yiHuo(ss);
		System.out.println();
		System.out.println(yiHuo);
	}
	
	public static String yiHuo(String in){
		
		
			int i = 0, j = 0;
			
			int len = in.length();
			short inb[] = new short[len];
			for (i = 0; i < len; i++) {
				inb[i] = charToHex(in.charAt(i)); // 将String里的每一个char转换为Hex
			}
			
			for (short s : inb) {
				System.out.print(s+" ");
			}
			
			for (i = 0; i < len; i++) { // 将每两个Hex合并成一个byte
				inb[j] = (byte) (((inb[i] << 4) & 0x00f0) | ((inb[i + 1]) & 0x000f));
				i++;
				j++;
			}
			System.out.println();
			for (short s : inb) {
				System.out.print(s+" ");
			}
			
			byte temp = 0x00; // 校验值
			for (i = 0; i < len / 2; i++) { // 异或
				temp ^= inb[i];
			}
			
			System.out.println();
			for (short s : inb) {
				System.out.print(s+" ");
			}
			String format = String.format("%02x", new Integer(temp & 0xff));
			//System.out.printf("%x", temp);
			//System.out.println();
			//System.out.println(format);
			System.out.println(format);
			return format.toUpperCase();
			
		
	}

	static short charToHex(char x) { // 将单个char转换为Hex
		short result = 0;
		switch (x) {
		case 'a':
			result = 10;
			break;
		case 'b':
			result = 11;
			break;
		case 'c':
			result = 12;
			break;
		case 'd':
			result = 13;
			break;
		case 'e':
			result = 14;
			break;
		case 'f':
			result = 15;
			break;

		case 'A':
			result = 10;
			break;
		case 'B':
			result = 11;
			break;
		case 'C':
			result = 12;
			break;
		case 'D':
			result = 13;
			break;
		case 'E':
			result = 14;
			break;
		case 'F':
			result = 15;
			break;
		default:
			result = (short) Character.getNumericValue(x);
			break;
		}
		return result;
	}

	

}
