package demo;

public class Singleton {
	
	private static  Singleton singleton;
	
	public static Singleton getinstince(){
		if (singleton==null) {
			singleton=new Singleton();
		}
		
		return singleton;
		
	}
	
	public static void main(String[] args) {
		int[] a={1,3,4,9,7,6,5,8,2};
		
		quickSort01(a,0, a.length-1);
		
		for (int i : a) {
			System.out.println(i);
		}
		bubbleSort(a);
		
	}
	
	public static void quickSort01(int[] a, int start, int end) {
	    if(start >= end)
	        return;
	    int i = start;
	    int j = end;
	    int base = a[start];
	    while(i != j) {
	        while(a[j] >= base && j > i)
	            j--;
	        while(a[i] <= base && i < j)
	            i++;
	        if(i < j) {
	            int temp = a[i];
	            a[i] = a[j];
	            a[j] = temp;
	        }
	    }
	    a[start] = a[i];
	    a[i] = base;
	    quickSort01(a, start, i - 1);
	    quickSort01(a, i + 1, end);
	}
	
	public static void bubbleSort(int[] a) {
	    int temp;
	    int size = a.length;
	    for(int i=1; i<size; i++) {
	        for(int j=0; j<size-i; j++) {
	            if(a[j] < a[j+1]) {
	                temp = a[j];
	                a[j]=a[j+1];
	                a[j+1]=temp;
	            }
	        }
	        for(int aa : a)
	            System.out.print(aa+",");
	        System.out.println();
	    }
	}

}
