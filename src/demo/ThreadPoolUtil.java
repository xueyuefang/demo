package demo;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPoolUtil {
	
	
	

	    private static volatile ExecutorService executorService;

	    ThreadPoolUtil() {}
	    
	    
	    
	    

	    public static ExecutorService getInstance() {
	        if (executorService == null) {
	            synchronized (ThreadPoolUtil.class) {
	                if (executorService == null) {
	                	executorService = Executors.newFixedThreadPool(5);
	                }
	            }
	        }
	        return executorService;
	    }
	}


