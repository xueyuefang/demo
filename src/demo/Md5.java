package demo;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Md5 {
	
	public static void main(String[] args) {
		long currentTimeMillis = System.currentTimeMillis();
		System.out.println(currentTimeMillis);
		String ss="20180232342810041adfqrouijasdfwefdfsdf"+currentTimeMillis;
		String md5 = getMD5(ss);
		System.out.println(md5);
		System.out.println(md5.length());
		String md5WithSalt = getMD5WithSalt(ss);
		System.out.println(md5WithSalt);
	}
	
	
	
	public static String getMD5(String content) {
	       try {
	           MessageDigest digest = MessageDigest.getInstance("MD5");
	           digest.update(content.getBytes());
	           return getHashString(digest);
	       } catch (NoSuchAlgorithmException e) {
	           e.printStackTrace();
	       }
	       return null;
	   }

	   private static final String SALT = "e01e10cd9593436d38ebd5bf1a453cd7";

	   /**
	    * 获取加盐的MD5字符串
	    */
	   public static String getMD5WithSalt(String content) {
	       return getMD5(getMD5(content) + SALT);
	   }

	   private static String getHashString(MessageDigest digest) {
	       StringBuilder builder = new StringBuilder();
	       for (byte b : digest.digest()) {
	           builder.append(Integer.toHexString((b >> 4) & 0xf));
	           builder.append(Integer.toHexString(b & 0xf));
	       }
	       return builder.toString();
	   }


}
