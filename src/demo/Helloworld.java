package demo;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.security.auth.x500.X500Principal;

public class Helloworld {
	public static void main(String[] args) throws InterruptedException {
		final Timer timer = new Timer();
		TimerTask task = new TimerTask() {
			public void run() {
				System.out.println("计时任务");
				

			}
		};
		timer.schedule(task,5000, 2000); 
	}
}

class MyTask extends TimerTask {

	@Override
	// 在run方法中的语句就是定时任务执行时运行的语句。
	public void run() {
		System.out.println("Hello!! 现在是：" + new Date().toLocaleString());
	}

}
