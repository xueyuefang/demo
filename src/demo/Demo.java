package demo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Demo {

	public static void main(String[] args) throws ParseException {
		
		/*long currentTimestamp = getCurrentTimestamp();
		System.out.println(currentTimestamp);
		Date date=new Date();
		long time = date.getTime();
		System.out.println(date.toString());
		System.out.println(time);
		 String osName = System.getProperty("os.name");
		 System.out.println(osName);
		String user = System.getProperty("user.name");
		System.out.println(user);*/
		/*int xxx=2000;
		String result = String.format("%04x", xxx );
		
		System.out.println(result);*/
	/*	Integer totalFee=12;
		Integer ww=12;
		Integer topU=(totalFee+ww)/10;
		System.out.println(topU);*/
		String rever = rever("abc");
		System.out.println(rever);
		String replice = replice("ababab");
		System.out.println(replice);
		
		int parseInt = Integer.parseInt("123");
		System.out.println(parseInt);
		Calendar now = Calendar.getInstance();
		System.out.println("年: " + now.get(Calendar.YEAR));
		System.out.println("月: " + (now.get(Calendar.MONTH) + 1) + "");
		System.out.println("日: " + now.get(Calendar.DAY_OF_MONTH));
		System.out.println("时: " + now.get(Calendar.HOUR_OF_DAY));
		System.out.println("分: " + now.get(Calendar.MINUTE));
		System.out.println("秒: " + now.get(Calendar.SECOND));
		System.out.println("当前时间毫秒数：" + now.getTimeInMillis());
		System.out.println(now.getTime());
		Date d = new Date();
		System.out.println(d);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateNowStr = sdf.format(d);
		System.out.println("格式化后的日期：" + dateNowStr);
		
		Singleton getinstince = Singleton.getinstince();
		
		System.out.println(getinstince);

	}

	private static String autoGenericCode(int code, int num) {
		String result = "";
		// 保留num的位数
		// 0 代表前面补充0
		// num 代表长度为4
		// d 代表参数为正数型
		result = String.format("%04d", code );

		return result;
	}
	
	public static long getCurrentTimestamp() {  
        return System.currentTimeMillis()/1000;  
    }  
	
	public static String rever(String string){
		return new StringBuilder(string).reverse().toString();
	}

	public static String replice(String string){
		String replaceAll = string.replaceAll("a", "b");
		return replaceAll;
	}
	
	
	
}
