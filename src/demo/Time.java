package demo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Time {
	
	public static void main(String[] args) {
		
		Date date=new Date(1560823555337L);
		DateFormat format = new SimpleDateFormat("yy MM dd HH mm ss"); 
		String formatd = format.format(date);
		System.out.println(formatd);
		String[] split = formatd.split(" ");
		StringBuffer buffer = new StringBuffer();
		for (String string : split) {
			int int1 = Integer.parseInt(string);
			String string2 = String.format("%02x", int1);
			buffer.append(string2);
		}
		
		String string = buffer.toString();
		System.out.println(string);
		
	}

}
